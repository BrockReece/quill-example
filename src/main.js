import Vue from 'vue';
import App from './App';

import 'quill/dist/quill.snow.css';

Vue.use(require('vue-quill'));

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App },
});
